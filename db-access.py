#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import mysql.connector


# Esta conexión con la IP del cliente está permitida en los ajustes del
# servidor de MySQL en Azure.


# Conexión con la base de datos MySQL en Azure
cnx = mysql.connector.connect(user="adminuser@mcwpmysqlserver", password="!ACB321..!",
    host="mcwpmysqlserver.mysql.database.azure.com", port=3306, database="wordpress")


# Se seleccionan todos los comentarios de la tabla de comentarios
sql_select_Query = "select * from wp_comments"
cursor = cnx.cursor()
cursor.execute(sql_select_Query)
records = cursor.fetchall()
print("Total number of rows in wp_comments is: ", cursor.rowcount)


# Se imprimen todos los comentarios y parte de su información
print("\nPrinting each comment record")
print("------------------------------------------\n")
for row in records:
    print("Comment ID = ", row[0], )
    print("Comment Post ID = ", row[1])
    print("Comment Author Email  = ", row[3])
    print("Comment Date = ", row[6])
    print("Comment Content = '" + row[8] + "'\n")
