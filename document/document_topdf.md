
---

# ICS - Despliegue de una aplicación con servicios de WordPress, Redis y base de datos en Azure

---

<br><br><br><br><br><br>

---
## 1. Descripción de los despliegues
---

Este documento se organiza en varias partes que reflejan dos configuraciones de despliegue distintas realizadas sobre Azure:

  * En la primera parte se muestra cómo hacer el despliegue de una aplicación con múltiples contenedores con **Docker Compose**. Los servicios de esta aplicación estarán intercomunicados y serán uno de WordPress y otro de base de datos. El servicio de WordPress se comunicará con el de base de datos (de MySQL) para almacenar y recuperar la información de los posts, los comentarios, etc. En la imagen inferior se puede ver el esquema general.

<br>
<p align="center">
  <img width="400" src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/despliegues_2.png">
</p>
<br>

  * En la segunda parte se utilizará también Docker Compose, pero los servicios serán WordPress y Redis, con una base de datos de MySQL de Azure separada. En esta configuración, el servicio de Redis y el de WordPress se comunicarán entre sí, y este útlimo con el servidor de base de datos de Azure. En la imagen que se muestra a continuación se puede ver el esquema general.

<br>
<p align="center">
  <img width="550" src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/despliegues_1.png">
</p>

<br><br>

### Archivos necesarios y URLs

El repositorio con todos los archivos necesarios para hacer los despliegues se encuentra en la siguiente URL: <https://gitlab.com/j.morano/ics-azure>. Ahí encontraremos tanto los archivos YAML para el Docker Compose como los demás archivos a los que se hará referencia a lo largo del documento. Para seguir los pasos, es necesario clonar este repositorio o descargar los archivos a los que se vaya haciendo referencia.

Cabe mencionar que el contenido de este repositorio está basado en el repositorio disponible en <https://github.com/Azure-Samples/multicontainerwordpress>.

La URL de la aplicación de WordPress desplegada es la siguiente: [mcwordpressapp.azurewebsites.net](http://mcwordpressapp.azurewebsites.net/) (base de datos en mcwpmysqlserver.mysql.database.azure.com).

<br><br><br>

---
## 2. Configuración y pasos de los despliegues
---

<br>

---
### 2.1. Creación de la aplicación web de WordPress con múltiples contenedores
---

Antes de poder iniciar la configuración de los despliegues, es necesario ejecutar el *login* en Azure:

```bash
az login
```

La respuesta a la ejecución del comando anterior es la que se puede ver a continuación:

```bash
morano@MS-7B98 ~/MCWP> az login
You have logged in. Now let us find all the subscriptions to which you have access...
[
  {
    "cloudName": "AzureCloud",
    "id": "09a29215-780d-4f31-8e88-ee610e23fee7",
    "isDefault": true,
    "name": "Evaluación gratuita",
    "state": "Enabled",
    "tenantId": "cea1ea3e-60b2-4f75-a6c2-a6022e8f961b",
    "user": {
      "name": "j.morano@udc.es",
      "type": "user"
    }
  }
]
```

<br>

---

Después de ejecutar el *login*, se crea, con el comando que se muestra a continuación, el **grupo de recursos** que utilizaremos posteriormente en la creación de los diferentes recursos, al que llamaremos `jmResourceGroup`:

```bash
az group create --name jmResourceGroup --location "West Europe"
```

La respuesta obtenida al ejecutar el comando anterior es la siguiente:

```bash
morano@MS-7B98 ~/MCWP> az group create --name jmResourceGroup --location "West Europe"
{
  "id": "/subscriptions/09a29215-780d-4f31-8e88-ee610e23fee7/resourceGroups/jmResourceGroup",
  "location": "westeurope",
  "managedBy": null,
  "name": "jmResourceGroup",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null,
  "type": "Microsoft.Resources/resourceGroups"
}
```

<br>

---

Una vez se tiene el grupo de recursos, es necesario crear un **plan de servicio de Azure**. Este plan de servicio define un conjunto de recursos de proceso que posibilitan la ejecución de una o más aplicaciones. En este caso se ha escogido el *Azure Service Plan S1*, que es el plan de servicio estándar más básico que ofrece Azure. Además de esto, definimos que para este plan de servicio se utilizará un contenedor Linux.

![wp1](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/S1.png)

El comando para crear un plan de servicio con las dos características mencionadas es el siguiente:

```bash
az appservice plan create --name jmAppServicePlan --resource-group jmResourceGroup --sku S1 --is-linux
```

Respuesta al comando:

```bash
morano@MS-7B98 ~/MCWP> az appservice plan create --name jmAppServicePlan --resource-group jmResourceGroup --sku S1 --is-linux
{
  "freeOfferExpirationTime": null,
  "geoRegion": "West Europe",
  "hostingEnvironmentProfile": null,
  "hyperV": false,
  "id": "/subscriptions/09a29215-780d-4f31-8e88-ee610e23fee7/resourceGroups/jmResourceGroup/providers/Microsoft.Web/serverfarms/jmAppServicePlan",
  "isSpot": false,
  "isXenon": false,
  "kind": "linux",
  "location": "West Europe",
  "maximumElasticWorkerCount": 1,
  "maximumNumberOfWorkers": 10,
  "name": "jmAppServicePlan",
  "numberOfSites": 0,
  "perSiteScaling": false,
  "provisioningState": "Succeeded",
  "reserved": true,
  "resourceGroup": "jmResourceGroup",
  "sku": {
    "capabilities": null,
    "capacity": 1,
    "family": "S",
    "locations": null,
    "name": "S1",
    "size": "S1",
    "skuCapacity": null,
    "tier": "Standard"
  },
  "spotExpirationTime": null,
  "status": "Ready",
  "subscription": "09a29215-780d-4f31-8e88-ee610e23fee7",
  "tags": null,
  "targetWorkerCount": 0,
  "targetWorkerSizeId": 0,
  "type": "Microsoft.Web/serverfarms",
  "workerTierName": null
}
```

<br><br>

Una vez que ya se tiene el plan de servicio creado, al que se le ha asignado el nombre `jmAppServicePlan`, se puede crear la **aplicación Docker con múltiples contenedores**, uno para **WordPress** y otro para la **base de datos**. Esto se hace con la herramienta **Docker Compose**. El comando ejecutado para ello es el siguiente:

```bash
az webapp create --resource-group jmResourceGroup --plan jmAppServicePlan --name MCWordPressApp --multicontainer-config-type compose --multicontainer-config-file docker-compose-wordpress.yml
```

La información necesaria que se ve reflejada en este comando es el plan de servicio (el que se ha creado antes), el nombre de la aplicación (`MCWordPressApp`), el tipo de configuración del multicontenedor (`compose`) y el fichero de configuración con la información del *compose* (un fichero YAML: `docker-compose-wordpress.yml`). Este último fichero se encuentra disponible en el respositorio y tiene el siguiente contenido:

```yml
version: '3.3'

services:
   db:
     image: mysql:5.7
     volumes:
       - db_data:/var/lib/mysql
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: somewordpress
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: wordpress

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: wordpress
volumes:
    db_data:
```

<br>

Como se puede ver, se están definiendo 2 servicios, uno de base de datos MySQL y otro de WordPress. Estos servicios se comunicarán entre ellos, de forma que los datos de la página de WordPress (e.g. los comentarios de los *posts* o los *posts* mismos) se almacenarán en la base de datos definida en el archivo YAML como `wordpress`. En este archivo también se definen los volúmenes necesarios para la base de datos y las imágenes de cada uno de los servicios.

La respuesta a la ejecución de la creación de la **aplicación web** con *Docker Compose* es la siguiente:

```bash
morano@MS-7B98 ~/MCWP> az webapp create --resource-group jmResourceGroup --plan jmAppServicePlan --name MCWordPressApp --multicontainer-config-type compose --multicontainer-config-file docker-compose-wordpress.yml
{
  "availabilityState": "Normal",
  "clientAffinityEnabled": true,
  "clientCertEnabled": false,
  "clientCertExclusionPaths": null,
  "cloningInfo": null,
  "containerSize": 0,
  "dailyMemoryTimeQuota": 0,
  "defaultHostName": "mcwordpressapp.azurewebsites.net",
  "enabled": true,
  "enabledHostNames": [
    "mcwordpressapp.azurewebsites.net",
    "mcwordpressapp.scm.azurewebsites.net"
  ],
  "ftpPublishingUrl": "ftp://waws-prod-am2-153.ftp.azurewebsites.windows.net/site/wwwroot",
  "geoDistributions": null,
  "hostNameSslStates": [
    {
      "hostType": "Standard",
      "ipBasedSslResult": null,
      "ipBasedSslState": "NotConfigured",
      "name": "mcwordpressapp.azurewebsites.net",
      "sslState": "Disabled",
      "thumbprint": null,
      "toUpdate": null,
      "toUpdateIpBasedSsl": null,
      "virtualIp": null
    },
    {
      "hostType": "Repository",
      "ipBasedSslResult": null,
      "ipBasedSslState": "NotConfigured",
      "name": "mcwordpressapp.scm.azurewebsites.net",
      "sslState": "Disabled",
      "thumbprint": null,
      "toUpdate": null,
      "toUpdateIpBasedSsl": null,
      "virtualIp": null
    }
  ],
  "hostNames": [
    "mcwordpressapp.azurewebsites.net"
  ],
  "hostNamesDisabled": false,
  "hostingEnvironmentProfile": null,
  "httpsOnly": false,
  "hyperV": false,
  "id": "/subscriptions/09a29215-780d-4f31-8e88-ee610e23fee7/resourceGroups/jmResourceGroup/providers/Microsoft.Web/sites/MCWordPressApp",
  "identity": null,
  "inProgressOperationId": null,
  "isDefaultContainer": null,
  "isXenon": false,
  "kind": "app,linux,container",
  "lastModifiedTimeUtc": "2019-12-03T12:16:36.213333",
  "location": "West Europe",
  "maxNumberOfWorkers": null,
  "name": "MCWordPressApp",
  "outboundIpAddresses": "52.166.113.188,52.232.70.113,52.233.178.192,104.40.210.169,104.45.21.209",
  "possibleOutboundIpAddresses": "52.166.113.188,52.232.70.113,52.233.178.192,104.40.210.169,104.45.21.209,51.144.106.224,51.144.53.68",
  "redundancyMode": "None",
  "repositorySiteName": "MCWordPressApp",
  "reserved": true,
  "resourceGroup": "jmResourceGroup",
  "scmSiteAlsoStopped": false,
  "serverFarmId": "/subscriptions/09a29215-780d-4f31-8e88-ee610e23fee7/resourceGroups/jmResourceGroup/providers/Microsoft.Web/serverfarms/jmAppServicePlan",
  "siteConfig": null,
  "slotSwapStatus": null,
  "state": "Running",
  "suspendedTill": null,
  "tags": null,
  "targetSwapSlot": null,
  "trafficManagerHostNames": null,
  "type": "Microsoft.Web/sites",
  "usageState": "Normal"
}
```

<br><br>

Después de un tiempo, la página pasa a estar disponible. Al principio, lo que se muestra es la primera pantalla de instalación de WordPress. La URL de la página es aquella que aparece en las respuestas a las ejecuciones de algunos de los comandos anteriores: `mcwordpressapp.azurewebsites.net`.

![wp1](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png)


<br><br>

---
### 2.2. Creación de una base de datos de producción y conexión con la aplicación
---

En los pasos que aquí se muestran, se pasará a utilizar una base de datos (BD) MySQL de Azure (*Azure Database for MySQL*) en lugar del contenedor de BD creado anteriormente. Utilizar este servicio de Azure tiene muchas ventajas, como la escalabilidad, pero es un servicio de pago, por lo que consumirá parte del crédito de bienvenida de 170 euros.

Para crear el servidor de base se datos de MySQL es necesario definir un usuario administrador (aquí `adminuser`) y una contraseña para este usuario (`!ACB321..!`), además de la capacidad de computación y de la versión utilizada. El comando que permite configurar todo esto es el siguiente:

```bash
az mysql server create --resource-group jmResourceGroup --name MCWPMySQLServer  --location "West Europe" --admin-user adminuser --admin-password !ACB321..! --sku-name B_Gen5_1 --version 5.7
```

La respuesta a la ejecución del comando:

```bash
morano@MS-7B98 ~/MCWP> az mysql server create --resource-group jmResourceGroup --name MCWPMySQLServer  --location "West Europe" --admin-user adminuser --admin-password !ACB321..! --sku-name B_Gen5_1 --version 5.7
{
  "administratorLogin": "adminuser",
  "earliestRestoreDate": "2019-12-03T13:01:25.153000+00:00",
  "fullyQualifiedDomainName": "mcwpmysqlserver.mysql.database.azure.com",
  "id": "/subscriptions/09a29215-780d-4f31-8e88-ee610e23fee7/resourceGroups/jmResourceGroup/providers/Microsoft.DBforMySQL/servers/mcwpmysqlserver",
  "location": "westeurope",
  "masterServerId": "",
  "name": "mcwpmysqlserver",
  "replicaCapacity": 5,
  "replicationRole": "None",
  "resourceGroup": "jmResourceGroup",
  "sku": {
    "capacity": 1,
    "family": "Gen5",
    "name": "B_Gen5_1",
    "size": null,
    "tier": "Basic"
  },
  "sslEnforcement": "Enabled",
  "storageProfile": {
    "backupRetentionDays": 7,
    "geoRedundantBackup": "Disabled",
    "storageAutoGrow": "Enabled",
    "storageAutogrow": null,
    "storageMb": 5120
  },
  "tags": null,
  "type": "Microsoft.DBforMySQL/servers",
  "userVisibleState": "Ready",
  "version": "5.7"
}
```

<br><br>

Después de crear el servidor de BD se pueden establecer las reglas del *firewall* del mismo, de modo que el acceso a la BD no esté disponible para cualquier máquina. En este caso, si se establece una regla con `0.0.0.0` como IPs fuente y destino se consigue limitar las conexiones con el servidor a conexiones con otros recursos de Azure.

El comando para establecer esta regla, y su respuesta, son los siguientes:

```bash
az mysql server firewall-rule create --name allAzureIPs --server MCWPMySQLServer --resource-group jmResourceGroup --start-ip-address 0.0.0.0 --end-ip-address 0.0.0.0
```

```bash
morano@MS-7B98 ~/MCWP> az mysql server firewall-rule create --name allAzureIPs --server MCWPMySQLServer --resource-group jmResourceGroup --start-ip-address 0.0.0.0 --end-ip-address 0.0.0.0
{
  "endIpAddress": "0.0.0.0",
  "id": "/subscriptions/09a29215-780d-4f31-8e88-ee610e23fee7/resourceGroups/jmResourceGroup/providers/Microsoft.DBforMySQL/servers/mcwpmysqlserver/firewallRules/allAzureIPs",
  "name": "allAzureIPs",
  "resourceGroup": "jmResourceGroup",
  "startIpAddress": "0.0.0.0",
  "type": "Microsoft.DBforMySQL/servers/firewallRules"
}
```

<br><br>

Una vez creado el servidor de BD, es necesario crear una BD. Como el servidor es un servidor de bases de datos MySQL, se creará una BD de este tipo. Para poder hacerlo, lo único que hay que indicar es el grupo de recursos, el nombre del servidor de base de datos (el creado anteriormente) y el nombre de la propia base de datos a crear, que en este caso es `wordpress`.

```bash
az mysql db create --resource-group jmResourceGroup --server-name MCWPMySQLServer --name wordpress
```

```bash
morano@MS-7B98 ~/MCWP> az mysql db create --resource-group jmResourceGroup --server-name MCWPMySQLServer --name wordpress
{
  "charset": "latin1",
  "collation": "latin1_swedish_ci",
  "id": "/subscriptions/09a29215-780d-4f31-8e88-ee610e23fee7/resourceGroups/jmResourceGroup/providers/Microsoft.DBforMySQL/servers/mcwpmysqlserver/databases/wordpress",
  "name": "wordpress",
  "resourceGroup": "jmResourceGroup",
  "type": "Microsoft.DBforMySQL/servers/databases"
}
```

<br><br>

Para hacer que WordPress se conecte con el servidor de base de datos y la base de datos creados es necesario configurar la aplicación de WordPress indicando el administrador, su contraseña, el *host* con la base de datos y el nombre de la base de datos. En este caso, se incluye también un certificado, necesario para la conexión segura mediante SSL de WordPress con la base de datos.

```bash
az webapp config appsettings set --resource-group jmResourceGroup --name MCWordPressApp --settings WORDPRESS_DB_HOST="MCWPMySQLServer.mysql.database.azure.com" WORDPRESS_DB_USER="adminuser@MCWPMySQLServer" WORDPRESS_DB_PASSWORD="!ACB321..!" WORDPRESS_DB_NAME="wordpress" MYSQL_SSL_CA="BaltimoreCyberTrustroot.crt.pem"
```

```bash
morano@MS-7B98 ~/MCWP> az webapp config appsettings set --resource-group jmResourceGroup --name MCWordPressApp --settings WORDPRESS_DB_HOST="MCWPMySQLServer.mysql.database.azure.com" WORDPRESS_DB_USER="adminuser@MCWPMySQLServer" WORDPRESS_DB_PASSWORD="!ACB321..!" WORDPRESS_DB_NAME="wordpress" MYSQL_SSL_CA="BaltimoreCyberTrustroot.crt.pem"
[
  {
    "name": "WORDPRESS_DB_HOST",
    "slotSetting": false,
    "value": "MCWPMySQLServer.mysql.database.azure.com"
  },
  {
    "name": "WORDPRESS_DB_USER",
    "slotSetting": false,
    "value": "adminuser@MCWPMySQLServer"
  },
  {
    "name": "WORDPRESS_DB_PASSWORD",
    "slotSetting": false,
    "value": "!ACB321..!"
  },
  {
    "name": "WORDPRESS_DB_NAME",
    "slotSetting": false,
    "value": "wordpress"
  },
  {
    "name": "MYSQL_SSL_CA",
    "slotSetting": false,
    "value": "BaltimoreCyberTrustroot.crt.pem"
  }
]
```

<br><br> 

Para que los cambios tengan efecto, se actualiza la configuración de la aplicación web con la nueva configuración añadida de conexión con la base de datos. Al utilizar la nueva base de datos de Azure, ya no es necesario el servicio de base de datos que teníamos definido anteriormente, por lo que se puede eliminar todo lo que tenía relación con él. Por otra parte, para poder usar *Azure Database for MySQL*, que usa SSL, es necesaria una configuración extra que no se encuentra en la imagen oficial de WordPress. Así, en lugar de esta imagen oficial (`wordpress:latest` en el archivo YAML) se utiliza una ya preparada con la configuración necesaria.

Esta imagen utilizada para el despliegue (fichero `Dockerfile`) es como la que se encuentra en el respositorio que se menciona al comienzo de esta sección. Para reflejar todos estos cambios, el archivo YAML (`compose-wordpress.yml`) queda del siguiente modo:

```yaml
version: '3.3'

services:
   wordpress:
     image: microsoft/multicontainerwordpress
     ports:
       - "8000:80"
     restart: always
```

<br>

Para actualizar la aplicación con todos estos cambios, se ejecuta el siguiente comando, estableciendo la nueva configuración:

```bash
az webapp config container set --resource-group jmResourceGroup --name MCWordPressApp --multicontainer-config-type compose --multicontainer-config-file compose-wordpress.yml
```

```bash
morano@MS-7B98 ~/MCWP> az webapp config container set --resource-group jmResourceGroup --name MCWordPressApp --multicontainer-config-type compose --multicontainer-config-file compose-wordpress.yml
[
  {
    "name": "DOCKER_CUSTOM_IMAGE_NAME",
    "value": "COMPOSE|dmVyc2lvbjogJzMuMycKCnNlcnZpY2VzOgogICBkYjoKICAgICBpbWFnZTogbXlzcWw6NS43CiAgICAgdm9sdW1lczoKICAgICAgIC0gZGJfZGF0YTovdmFyL2xpYi9teXNxbAogICAgIHJlc3RhcnQ6IGFsd2F5cwogICAgIGVudmlyb25tZW50OgogICAgICAgTVlTUUxfUk9PVF9QQVNTV09SRDogc29tZXdvcmRwcmVzcwogICAgICAgTVlTUUxfREFUQUJBU0U6IHdvcmRwcmVzcwogICAgICAgTVlTUUxfVVNFUjogd29yZHByZXNzCiAgICAgICBNWVNRTF9QQVNTV09SRDogd29yZHByZXNzCgogICB3b3JkcHJlc3M6CiAgICAgZGVwZW5kc19vbjoKICAgICAgIC0gZGIKICAgICBpbWFnZTogd29yZHByZXNzOmxhdGVzdAogICAgIHBvcnRzOgogICAgICAgLSAiODAwMDo4MCIKICAgICByZXN0YXJ0OiBhbHdheXMKICAgICBlbnZpcm9ubWVudDoKICAgICAgIFdPUkRQUkVTU19EQl9IT1NUOiBkYjozMzA2CiAgICAgICBXT1JEUFJFU1NfREJfVVNFUjogd29yZHByZXNzCiAgICAgICBXT1JEUFJFU1NfREJfUEFTU1dPUkQ6IHdvcmRwcmVzcwp2b2x1bWVzOgogICAgZGJfZGF0YToK"
  }
]
```

<br><br>

Después de este proceso, el funcionamiento de la página sigue siendo el mismo, pero la base de datos ahora se sitúa fuera de la aplicación, en un servidor de base de datos aparte.

<p align="center">
  <img width="550" src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_2.png">
</p>

<!-- ![wp2](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_2.png) -->

<br><br><br>

---

### 2.3. Persistencia y servicio Redis

---

<br>

#### Persistencia

Una vez que se tiene el servicio de WordPress conectado con la base de datos ubicada en el servidor de base de datos de Azure, se puede añadir un **almacenamiento persistente**. De esta forma, se evita la pérdida de la instalación de WordPress en el caso de que haya un reinicio de la aplicación. Este almacenamiento persistente se consigue habilitando en la configuración de la aplicación de WordPress la funcionalidad de persistencia y editando el fichero YAML del modo que se muestra posteriormente.

Este es el comando para habilitar el almacenamiento persistente en la aplicación de WordPress:

```bash
az webapp config appsettings set --resource-group jmResourceGroup --name MCWordPressApp --settings WEBSITES_ENABLE_APP_SERVICE_STORAGE=TRUE
```

```bash
morano@MS-7B98 ~/MCWP> az webapp config appsettings set --resource-group jmResourceGroup --name MCWordPressApp --settings WEBSITES_ENABLE_APP_SERVICE_STORAGE=TRUE
[
  {
    "name": "WORDPRESS_DB_HOST",
    "slotSetting": false,
    "value": "MCWPMySQLServer.mysql.database.azure.com"
  },
  {
    "name": "WORDPRESS_DB_USER",
    "slotSetting": false,
    "value": "adminuser@MCWPMySQLServer"
  },
  {
    "name": "WORDPRESS_DB_PASSWORD",
    "slotSetting": false,
    "value": "!ACB321..!"
  },
  {
    "name": "WORDPRESS_DB_NAME",
    "slotSetting": false,
    "value": "wordpress"
  },
  {
    "name": "MYSQL_SSL_CA",
    "slotSetting": false,
    "value": "BaltimoreCyberTrustroot.crt.pem"
  },
  {
    "name": "WEBSITES_ENABLE_APP_SERVICE_STORAGE",
    "slotSetting": false,
    "value": "TRUE"
  }
]
```

<br>

El archivo `compose-wordpress.yml`, con los nuevos cambios, es este:

```yaml
version: '3.3'

services:
   wordpress:
     image: microsoft/multicontainerwordpress
     volumes:
      - ${WEBAPP_STORAGE_HOME}/site/wwwroot:/var/www/html
     ports:
       - "8000:80"
     restart: always
```

Las nuevas líneas añadidas (*volumes* e inferior) permiten *mapear* el almacenamiento de la aplicación a un almacenamiento persistente.

<br><br>

De nuevo, para que lo cambios tengan efecto, se actualiza la configuración usando el nuevo fichero YAML.

```bash
az webapp config container set --resource-group jmResourceGroup --name MCWordPressApp --multicontainer-config-type compose --multicontainer-config-file compose-wordpress.yml
```

```bash
morano@MS-7B98 ~/MCWP> az webapp config container set --resource-group jmResourceGroup --name MCWordPressApp --multicontainer-config-type compose --multicontainer-config-file compose-wordpress.yml
[
  {
    "name": "WEBSITES_ENABLE_APP_SERVICE_STORAGE",
    "slotSetting": false,
    "value": "TRUE"
  },
  {
    "name": "DOCKER_CUSTOM_IMAGE_NAME",
    "value": "COMPOSE|dmVyc2lvbjogJzMuMycKCnNlcnZpY2VzOgogICBkYjoKICAgICBpbWFnZTogbXlzcWw6NS43CiAgICAgdm9sdW1lczoKICAgICAgIC0gZGJfZGF0YTovdmFyL2xpYi9teXNxbAogICAgIHJlc3RhcnQ6IGFsd2F5cwogICAgIGVudmlyb25tZW50OgogICAgICAgTVlTUUxfUk9PVF9QQVNTV09SRDogc29tZXdvcmRwcmVzcwogICAgICAgTVlTUUxfREFUQUJBU0U6IHdvcmRwcmVzcwogICAgICAgTVlTUUxfVVNFUjogd29yZHByZXNzCiAgICAgICBNWVNRTF9QQVNTV09SRDogd29yZHByZXNzCgogICB3b3JkcHJlc3M6CiAgICAgZGVwZW5kc19vbjoKICAgICAgIC0gZGIKICAgICBpbWFnZTogd29yZHByZXNzOmxhdGVzdAogICAgIHBvcnRzOgogICAgICAgLSAiODAwMDo4MCIKICAgICByZXN0YXJ0OiBhbHdheXMKICAgICBlbnZpcm9ubWVudDoKICAgICAgIFdPUkRQUkVTU19EQl9IT1NUOiBkYjozMzA2CiAgICAgICBXT1JEUFJFU1NfREJfVVNFUjogd29yZHByZXNzCiAgICAgICBXT1JEUFJFU1NfREJfUEFTU1dPUkQ6IHdvcmRwcmVzcwp2b2x1bWVzOgogICAgZGJfZGF0YToK"
  }
]
```

<br><br>

#### Servicio Redis

Para añadir un servicio Redis que se comunique con la aplicación de WordPress, ampliamente utilizado en aplicaciones web para hacer *caching* persistente de objetos, son necesarios una serie de pasos. Al igual que se hizo con el servicio de base de datos al principio del documento, se utilizará **Docker Compose**.

El primero de los pasos a seguir es configurar la aplicación por medio del siguiente comando, en el que se le indica a WordPress el host de Redis:

```bash
az webapp config appsettings set --resource-group jmResourceGroup --name MCWordPressApp --settings WP_REDIS_HOST="redis"
```

```bash
morano@MS-7B98 ~/M/I/A/P/q/MCWP> az webapp config appsettings set --resource-group jmResourceGroup --name MCWordPressApp --settings WP_REDIS_HOST="redis"
[
  {
    "name": "WORDPRESS_DB_HOST",
    "slotSetting": false,
    "value": "MCWPMySQLServer.mysql.database.azure.com"
  },
  {
    "name": "WORDPRESS_DB_USER",
    "slotSetting": false,
    "value": "adminuser@MCWPMySQLServer"
  },
  {
    "name": "WORDPRESS_DB_PASSWORD",
    "slotSetting": false,
    "value": "!ACB321..!"
  },
  {
    "name": "WORDPRESS_DB_NAME",
    "slotSetting": false,
    "value": "wordpress"
  },
  {
    "name": "MYSQL_SSL_CA",
    "slotSetting": false,
    "value": "BaltimoreCyberTrustroot.crt.pem"
  },
  {
    "name": "WP_REDIS_HOST",
    "slotSetting": false,
    "value": "redis"
  }
]
```

<br>

Como segundo paso (aunque puede realizarse antes o después del paso anterior) hay que modificar el fichero YAML, añadiendo el nuevo servicio Redis tal y como se muestra a continuación:

```yaml
version: '3.3'

services:
   wordpress:
     image: microsoft/multicontainerwordpress
     volumes:
      - ${WEBAPP_STORAGE_HOME}/site/wwwroot:/var/www/html
     ports:
       - "8000:80"
     restart: always

   redis:
     image: redis:3-alpine
     restart: always
```

<br>

Una vez realizados estos dos pasos, hay que actualizar de nuevo la configuración con el nuevo archivo YAML.

```bash
morano@MS-7B98 ~/M/I/A/P/q/MCWP> az webapp config container set --resource-group jmResourceGroup --name MCWordPressApp --multicontainer-config-type compose --multicontainer-config-file compose-wordpress.yml

[
  {
    "name": "DOCKER_CUSTOM_IMAGE_NAME",
    "value": "COMPOSE|dmVyc2lvbjogJzMuMycKCnNlcnZpY2VzOgogICB3b3JkcHJlc3M6CiAgICAgaW1hZ2U6IG1pY3Jvc29mdC9tdWx0aWNvbnRhaW5lcndvcmRwcmVzcwogICAgIHBvcnRzOgogICAgICAgLSAiODAwMDo4MCIKICAgICByZXN0YXJ0OiBhbHdheXMKCiAgIHJlZGlzOgogICAgIGltYWdlOiByZWRpczozLWFscGluZQogICAgIHJlc3RhcnQ6IGFsd2F5cwo="
  }
]
```

<br>

Llegados a este punto ya se tiene una aplicación de WordPress con Redis (desplegados con Docker Compose) conectada a una base de datos de MySQL ubicada en un servidor de base de datos de Azure.

<br>

---
### 2.4. Instalación de WordPress y comprobación de buen funcionamiento
---

Al realizar todos los pasos descritos en los apartados anteriores, en el panel de Azure se pueden ver 3 recursos: el plan de servicio, la aplicación y el servidor de base de datos. Todos ellos están corriendo y no muestran ningún error. Además, el log del Docker Compose de la aplicación, con Redis y WordPress, indica que todo funciona correctamente.

<br>

<p align="center">
  <img width="700" src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/azure-resources.png">
</p>

<br>

<!-- ![az_r](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/azure-resources.png) -->
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

![wp3](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/docker_compose.png)


<br><br><br><br>

Para comprobar que estos recursos se están comunicando adecuadamente entre ellos, se procede ahora a la **instalación de WordPress**, cuyos pasos se muestran en las capturas de pantalla situadas a continuación.

<br>

<p align="center">
  <img width="680" src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_3.png">
</p>

<p align="center">
  <img width="680" src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_4_signup.png">
</p>


<!-- ![wp3](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_3.png) -->
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

<!-- ![wp4](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_4_signup.png) -->
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

<p align="center">
  <img width="680" src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_4_signup_success.png">
</p>

<p align="center">
  <img width="680" src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_5_login.png">
</p>


<!-- ![wp4_sss](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_4_signup_success.png) -->
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

<!-- ![wp5_li](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_5_login.png) -->
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->


<br><br><br><br><br><br>

Una vez iniciada sesión, la pantalla que se muestra es el *dashboard* de WordPress, donde se puede visualizar la página de WordPress y configurar todo lo que tenga que ver con ella, desde la apariencia hasta el contenido, pasando por su funcionamiento y los *plugins* de los que hace uso.

![wp6_db](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_6_dashboard.png)
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

<br>


<p align="center">
  <img width="600" src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_7_wordpress-main.png">
</p>
<!-- ![wp7_wpm](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_7_wordpress-main.png) -->
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

<br>

Si se observa la página principal, se puede ver que hay un único *post*: el de ejemplo, que se crea automáticamente durante la instalación.


<br>

En la sección de *plugins* del *dashboard* anterior podemos comprobar que el *plugin* de **Redis** ha sido añadido correctamente y que funciona, como reflejan las imágenes siguientes, por lo que se puede afirmar que la comunicación entre los dos servicios es adecuada.

<br>

![wp8_p](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_8_plugins.png)
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

![wp9_r](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_9_redis.png)
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

<br><br><br><br><br><br><br><br><br><br>

### 2.4. Comprobación de la base de datos

Para comprobar el buen funcionamiento de la base de datos y la buena comunicación con la aplicación de WordPress se han instalado un par de *plugins* de base de datos en WordPress y se ha implementado además un pequeño *script* en Python 3 que permite acceder, mediante autenticación, a la base de datos de comentarios e imprimir los comentarios que se han realizado en los *posts*.  

En la imagen que se muestra a continuación se puede observar que el *host* de la base de datos es el servidor creado para tal fin en uno de los pasos anteriores. También se puede observar que el nombre de la BD concuerda con el que se ha definido previamente y que se han creado de forma automática durante la instalación de WordPress múltiples tablas. Una de ellas, `wp_comments`, es la que utilizaremos para comprobar que la BD funciona correctamente y que además se está comunicando de forma adecuada con la aplicación de WordPress.

![az_r](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_10_database.png)
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

<br>

En esta imagen, en la tabla `wp_comments`, se puede ver que el número de registros es 3. Cada registro se corresponde con cada uno de los comentarios. Estos 3 comentarios pertenecen al único post de la página (post de ejemplo *Hello world*) y son un comentario que se lo ha generado WordPress automáticamente y otros 2 comentarios de prueba que se han introducido manualmente. Todos ellos se pueden ver en la imagen de la web que se muestra a continuación.

![az_r](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_11_comments.png)
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

<br>

Tal y como se puede observar, el número de comentarios y el número de registros mostrados en la base de datos concuerdan. Si además, con otro *plugin*, observamos el contenido de esos registros, podemos ver que también son coherentes con lo que se muestra en la página de WordPress.

![az_r](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_12_comments_table.png)
<!-- <img src="/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/webpage_1.png" width="500" /> -->

<br>

Para hacer una última comprobación, como se ha adelantado, se ha implementado un *script* en Python 3 que hace una petición a la base de datos del servidor. Concretamente, lo que hace el *script* es conectarse al servidor de base de datos y obtener, mediante una consulta SQL, todos los comentarios de la base de datos (tabla `wp_comments`). Una vez hecho esto, imprime todos ellos por pantalla.

El código del *script* es el siguiente:

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import mysql.connector


# Esta conexión con la IP del cliente está permitida en los ajustes del
# servidor de MySQL en Azure.


# Conexión con la base de datos MySQL en Azure
cnx = mysql.connector.connect(user="adminuser@mcwpmysqlserver", password="!ACB321..!",
    host="mcwpmysqlserver.mysql.database.azure.com", port=3306, database="wordpress")


# Se seleccionan todos los comentarios de la tabla de comentarios
sql_select_Query = "select * from wp_comments"
cursor = cnx.cursor()
cursor.execute(sql_select_Query)
records = cursor.fetchall()
print("Total number of rows in wp_comments is: ", cursor.rowcount)


# Se imprimen todos los comentarios y parte de su información
print("\nPrinting each comment record")
print("------------------------------------------\n")
for row in records:
    print("Comment ID = ", row[0], )
    print("Comment Post ID = ", row[1])
    print("Comment Author Email  = ", row[3])
    print("Comment Date = ", row[6])
    print("Comment Content = '" + row[8] + "'\n")
```

<br>

El resultado de su ejecución es el que se muestra a continuación, pudiéndose ver en lo que se imprime por pantalla los tres comentarios que se mostraban anteriormente: el de ejemplo y los otros dos añadidos manualmente a modo de prueba.

```bash
morano@MS-7B98 ~/M/I/A/P/q/MCWP> ./db-access.py
Total number of rows in wp_comments is:  3

Printing each comment record
------------------------------------------

Comment ID =  1
Comment Post ID =  1
Comment Author Email  =  wapuu@wordpress.example
Comment Date =  2019-12-03 14:58:30
Comment Content = 'Hi, this is a comment.
To get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.
Commenter avatars come from <a href="https://gravatar.com">Gravatar</a>.'

Comment ID =  2
Comment Post ID =  1
Comment Author Email  =  j.morano@udc.es
Comment Date =  2019-12-03 15:17:48
Comment Content = 'Comentario de prueba 1.'

Comment ID =  3
Comment Post ID =  1
Comment Author Email  =  j.morano@udc.es
Comment Date =  2019-12-03 16:07:49
Comment Content = 'Comentario de prueba 2.'
```

<br>

Para hacer este último paso, se ha añadido al *firewall* del servidor de base de datos la regla que se puede ver en la imagen inferior. Esta regla permite acceder a la base de datos desde la IP que se muestra en la imagen, que es la IP de la máquina desde donde se ha ejecutado el *script*.

![az_r](/home/morano/MUEI/ICS/Azure/P1/quickstart/MCWP/document/img/azure_mysql-conn-security.png)
